# Репозиторий мульти-скриптов Тимофея Казанцева (aka PianoIst, Levitanus)

##Как скачать
Каждый скрипт лежит в отдельной ветке репозитория. Для того, чтобы скачать скрипты:

*   Перейдите на вкладку **Downloads** в левой панели
*   Скачайте ветку **Master**

## Как установить
Скопировать содержимое папки Kontakt5 в папку ресурсов контакта.
Если вы не знаете, где папка ресурсов:

*   откройте панель мульти-скриптов
*   нажмите кнопку "Preset"
*   Выбирете "Save Preset..."
*   Перейдите в файловом менеджере в папку, в которую контакт предлагал сохранить пресет
*   поднимитесь до уровня папки "Kontakt5"
*   На windows эта папка находится по адресу: C:\Users\<пользователь>\Documents\Native Instruments\Kontakt 5

##Как пользоваться
Все скрипты снабжены *.md руководствами. Можно их прочитать в любой читалке\текстовом редакторе, поддерживающем markdown, или зайти на страницу исходного кода соответствующего скрипта. Для этого:

*   в левой панели перейдите на вкладку Source
*   Зайдите в папку Kontakt 5
*   Выбирете любой из *.md файлов
*   Зайдите в папку скрипта с помощью файлового браузера
